#include <iostream>
#include <unistd.h>
#include "Parser/includes/Parser.h"

using namespace std;
int main(int argc, char* argv[]) {

    if (argc != 3) {
        cout << "Bitte das Programm so aufrufen: " << "\n";
        cout << "./RunParser <input.txt> <output.txt>" << endl;

        return 0;
    }

    char* inputFile = argv[1];
    char* outputFile = argv[2];

    cout << "Input:  " << inputFile << endl;
    cout << "Output: " << outputFile << endl;
    cout << "------------------------" << endl << endl;

    try{
        Parser* p = new Parser(inputFile);
        p->parse(outputFile);
    } catch (const char* e) {
        cerr << "[" << e << "]" << endl;
        cerr << "stop" << endl;
    }

    cout << endl << "------------------------" << endl << endl;
    return 0;
}
