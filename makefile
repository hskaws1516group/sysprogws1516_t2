

OBJDIR = objs

PARSERDIR = Parser
TREEDIR = tree
PROJECTDIR = projectOBJs
SP1DIR = sysprog1objs

all: parserOBJs parseTreeOBJs mainOBJs

cleanAll:
	rm -f $(PARSERDIR)/$(OBJDIR)/*.o
	rm -f $(TREEDIR)/$(OBJDIR)/*.o
	rm -f RunParser



parserOBJs:
	$(MAKE) -C $(PARSERDIR) ParserOBJTarget

parseTreeOBJs:
	$(MAKE) -C $(TREEDIR) ParseTreeOBJTarget


mainOBJs: main.cpp
	g++ -std=c++11 -g -c -Wall -o $(PROJECTDIR)/Main.o main.cpp


#link everything
Parser: Parser/objs/Parser.o tree/objs/ParseTree.o
	g++ -std=c++11 -o RunParser Parser/objs/Parser.o tree/objs/ParseTree.o 

ParserAll:
	${MAKE} cleanAll
	${MAKE} all
	g++ -std=c++11 -o RunParser Parser/objs/Parser.o tree/objs/ParseTree.o $(SP1DIR)/ScannerLib.o ${PROJECTDIR}/Main.o

