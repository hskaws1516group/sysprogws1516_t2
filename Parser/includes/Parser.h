//
// Created by Dominik on 29.12.2015.
//

#ifndef SYSPROGWS1516_T2_PARSER_H
#define SYSPROGWS1516_T2_PARSER_H


#include "../includes/Parser.h"
#include "../../sysprog1includes/Scanner/includes/Scanner.h"
#include "../../sysprog1includes/Symboltable/includes/Symboltable.h"
#include "../../sysprog1includes/Token/includes/Token.h"
#include "../../tree/includes/ParseTreeNode.h"
namespace PR {
enum ParseResult {
		RULEFOUND = 0,
		RULENOTFOUND = 1,
		ERROR = 2,
	};
}
class Parser {
public:

    Parser(const char *inputfile);
    void parse(const char *outputfile);

private:
    ParseTreeNode* parseTree;

    Scanner* scanner;
    Symboltable* symtab;

    void Prog(ParseTreeNode* n);
    void Decls(ParseTreeNode* n);
    PR::ParseResult Decl(ParseTreeNode* n);

    void Array(ParseTreeNode* n);
    void Statements(ParseTreeNode* n);
    PR::ParseResult Statement(ParseTreeNode* n);
    PR::ParseResult Index(ParseTreeNode* n);
    void Exp(ParseTreeNode* n);
    void Exp2(ParseTreeNode* n);
    void OpExp(ParseTreeNode* n);
    PR::ParseResult Op(ParseTreeNode* n);
    Token* retrieveToken();
 bool checkTokenType(Token* token, Tokens::TokenType type);
    void error(char* errorMsg);
    Token* actualToken;

};

#endif //SYSPROGWS1516_T2_PARSER_H
