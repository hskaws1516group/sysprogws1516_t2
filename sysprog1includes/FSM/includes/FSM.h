#ifndef FSM_H_
#define FSM_H_


#include "../../Token/includes/Token.h"
#include "States/StateBase.h"

/**
 * Baseclass for a finite state machine.
 * This FSM is based on the state pattern.
 * This class contains  pure virtual functions
 */
class FSM {
public:
    /**
     * Creates a new instance of the FSM.
     */
    FSM();

    /**
     * Destroys the FSM and releases used ressources.
     */
    virtual ~FSM();

    /**
     * Sets the current state to the given state.
     * @param state the state object which should be the new current state.
     */
    void setCurrentState(StateBase *state) {

        currentState = state;
    }

    /**
     * Provides a char into the FSM. The char is now processed by the FSM and its states.
     * @param c the char to process
     */
    virtual void readChar(char c);

    /**
     * Returns a state that is associated with the given stateNr.
     * @param StateBase the state which corresponds to the stateNr.
     */
    virtual StateBase *provideState(int stateNr) = 0;

    /**
       * Indicates to go a specific number of chars back
       * @param nr the number of chars to go back
       */
    virtual void ungetChar(int nr) = 0;

    /**
     * Called if a token was recognized by the fsm.
     * The tokenType specifies the type of the token. It depends on which state
     * was the last state in the fsm.
     * @param tokenType the type of the recognized token
     */
    virtual void makeToken(Tokens::TokenCategory tokenType) = 0;

    /**
     * The length of the recognized token.
     */
    int tokenLength = 0;

protected:
    StateBase *currentState;
};

#endif /* FSM_H_ */
