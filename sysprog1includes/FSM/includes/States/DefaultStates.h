//
// Created by Dominik on 21.10.2015.
//

#ifndef AUTOMAT_DEFAULTSTATES_H
#define AUTOMAT_DEFAULTSTATES_H

#include "../Log.h"
#include "StateBase.h"

class Log;

/**
 * The InitState is used as start state for the LFSM. It contains transitions for sign, digits and letters
 * into the corresponding states.
 * \author Dominik Bartos
 */
class InitState: public StateBase {
public:

	/**
	 * Creates a new instance of InitState.
	 * @param FSM the fsm in which the state is used
	 */
	InitState(FSM *fsm) :
			StateBase(fsm) {

	}

	/**
	 * Destroys the InitState and releases used resources
	 */
	~InitState() {
	}
	;

	/**
	 * Reads a char and starts following transitions:<br>
	 * <pre>
	 * following state | triggering character
	 * 3                    signs\\{<,:}
	 * 4                    digit
	 * 5                    <
	 * 8                    :
	 * 13                   letter
	 * 14                   spaces, tabs, line breaks
	 * 1 (error)          sigma\\{sign, digit, letter} -> invalid character
	 * 2 (stop)             '\0' (null, represents EOF)
	 * </pre>
	 */
	void  readChar(char c) {

		Log::stateRead(stateNr, c);
		int asciiCode = (unsigned char) c;

		if (asciiCode == 33 || asciiCode == 38
				|| (asciiCode >= 40 && asciiCode <= 43) || asciiCode == 45
				|| asciiCode == 61 || asciiCode == 62 || asciiCode == 59
				|| asciiCode == 91 || asciiCode == 93 || asciiCode == 123
				|| asciiCode == 125) { //only accept: !,&,(,),*,+,-,;=,>,[,],{,}

			Log::stateReadResult(stateNr, 3, 1);
			fsm->setCurrentState(fsm->provideState(3));
		} else if (asciiCode >= 48 && asciiCode <= 57) { // 0-9
			Log::stateReadResult(stateNr, 4, 1);

			fsm->setCurrentState(fsm->provideState(4));
		} else if (asciiCode == 60) { //<, possible start of <:>
			Log::stateReadResult(stateNr, 5, 1);

			fsm->setCurrentState(fsm->provideState(5));
		} else if (asciiCode == 58) { // :
			Log::stateReadResult(stateNr, 8, 1);
			fsm->setCurrentState(fsm->provideState(8));
		} else if ((asciiCode >= 65 && asciiCode <= 90)
				|| (asciiCode >= 97 && asciiCode <= 122)) { //a-z, A-Z
			Log::stateReadResult(stateNr, 13, 1);
			fsm->setCurrentState(fsm->provideState(13));
		} else if (c == '\0') {
			//going into stop state
			Log::stateReadResult(stateNr, 2, 1);
			fsm->setCurrentState(fsm->provideState(2));
		} else if ( c == 32 || c == 10) { // Spaces, Breaks

			Log::stateReadResult(stateNr, 14, 1);
			fsm->setCurrentState(fsm->provideState(14));
		} else {
			//an invalid character was read, going into error state

			Log::stateReadResult(stateNr, 1, 1);
			fsm->setCurrentState(fsm->provideState(1));
		}

	}

};

/**
 * The ErrorState is used to express an occurred error in the FSM.
 * An error occurs basically if an invalid character was read by the FSM
 *  \author Dominik Bartos
 */
class ErrorState: public StateBase {
public:
	/**
	 * Creates a new instance of the ErrorState
	 * @param FSM the fsm in which the state is used
	 */
	ErrorState(FSM *fsm) :
			StateBase(fsm) {

	}

	~ErrorState() {
	}
	;
	/**
	 * Reads an char
	 */
	void readChar(char c) {

		Log::stateRead(stateNr, c);
		Log::stateReadResult(stateNr, -1, 0);
		fsm->ungetChar(1);
		fsm->makeToken(Tokens::TC_ERROR);
		fsm->setCurrentState(fsm->provideState(0));




	}
};

/**
 * The SpacesBreaksCharState is used for recognition of spaces and breaks.

 *  \author Dominik Bartos
 */
class SpacesBreaksCharState: public StateBase {
public:
	/**
	 * Creates a new instance of the SpacesBreaksCharState.
	 * This state is especially used to handle spaces and breaks
	 * @param FSM the fsm in which the state is used
	 */
	SpacesBreaksCharState(FSM *fsm) :
			StateBase(fsm) {

	}

	~SpacesBreaksCharState() {
	}
	;

	/**
	 * Reads a char and ungets it, if it is no space or break (because the read char must be read again)
	 * an creates A "NOTOKEN" token, to inform the FSM to reset the token data and start a new token.
	 *
	 */
	void readChar(char c) {

		Log::stateRead(stateNr, c);
		Log::stateReadResult(stateNr, -1, 0);

		if ( c == 32 || c == 10) { // Spaces, Breaks

			Log::stateReadResult(stateNr, 14, 1);
			fsm->setCurrentState(fsm->provideState(14));

		} else {
			fsm->ungetChar(1);
			//call make token here, to reset the counted length of the token in fsm
			fsm->makeToken(Tokens::TC_NOTOKEN);
			fsm->setCurrentState(fsm->provideState(0));


		}

	}
};

/**
 * The StopState is used to indicate that the FSM has stopped.
 * The FSM stops if it reads '\0' which represents end of file.
 *  \author Dominik Bartos
 */
class StopState: public StateBase {
public:
	/**
	 * Creates a new instance of the StopState
	 * @param The FSM which uses the state
	 *
	 */
	StopState(FSM *fsm) :
			StateBase(fsm) {

	}

	~StopState() {
	}
	;

	/**
	 * Reads a char and calls makeToken with NOTOKEN as type
	 */
	void readChar(char c) {

		Log::stateRead(stateNr, c);
		Log::stateReadResult(stateNr, -1, 0);

		fsm->makeToken(Tokens::TC_NOTOKEN);

	}
};

#endif //AUTOMAT_DEFAULTSTATES_H

