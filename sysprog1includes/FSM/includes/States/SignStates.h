//
// Created by Dominik on 21.10.2015.
//

#ifndef AUTOMAT_SIGNSTATES_H
#define AUTOMAT_SIGNSTATES_H


#include "../Log.h"

/**
 * State03 is one of the states responsible for recognizing signs
 * \author  Dominik Bartos
 */
class State03 : public StateBase {

public:
    /**
     * Creates a new instance of the state
     * @param FSM the FSM which uses the state
     *
     */
    State03(FSM *fsm) : StateBase(fsm) { }

    ~State03() { };

    /**
     * Every char which indicates sign except '<' and ':' will lead the FSM into this state.
     *
     * Reads a char and starts following transitions:<br>
     * <pre>
     * following state | triggering character
     * 0 (InitState)        every character
     * </pre>
     * A sign is a 'standalone' token, so if the FSM is in this state, it means that there must be
     * a SignToken created. This state will then communicate to the FSm to go one char back
     * and to make a SignToken. Afterwards the FSm is in the InitState and ready for new input.
     *
     */
    void readChar(char c) {


        Log::stateRead(stateNr, c);
        Log::stateReadResult(stateNr, -1, 0);
        fsm->ungetChar(1);
        fsm->makeToken(Tokens::TokenCategory::TC_SIGN);
        fsm->setCurrentState(fsm->provideState(0));


    }

};

/**
 * State05 is one of the states responsible for recognizing signs.
 * The character '<' will lead the FSM into this state.
 * '<' is a possible start of the compound sign '<:>'
 *
 * \author  Dominik Bartos
 */
class State05 : public StateBase {

public:
    /**
   * Creates a new instance of the state
   * @param FSM the FSM which uses the state
   *
   */
    State05(FSM *fsm) : StateBase(fsm) { }

    ~State05() { };

/**
 *
   * Reads a char and starts following transitions:<br>
     * <pre>
     * following state | triggering character
     * 6                    :
     * 0 (Init)             everything else
     * </pre>
     * If the next read char in this state is not ':', the state leads back to InitState
     * and causes the creation of a SignToken for '<'.
     *
 */
    void readChar(char c) {

        Log::stateRead(stateNr, c);
        //transition with ':' to state 6
        if (c == ':') {

            Log::stateReadResult(stateNr, 6, 1);
            fsm->setCurrentState(fsm->provideState(6));

        } else {
            Log::stateReadResult(stateNr, -1, 0);
            fsm->ungetChar(1);
            fsm->makeToken(Tokens::TokenCategory::TC_SIGN);
            fsm->setCurrentState(fsm->provideState(0));


        }
    }

};
/**
 * State06 is one of the states responsible for recognizing signs.
 * This state is required to recognize the compound sign '<:>'.
 * \author  Dominik Bartos
 */
class State06 : public StateBase {

public:
    /**
   * Creates a new instance of the state
   * @param FSM the FSM which uses the state
   *
   */
    State06(FSM *fsm) : StateBase(fsm) { }

    ~State06() { };
/**
 *
 *
   * Reads a char and starts following transitions:<br>
     * <pre>
     * following state | triggering character
     * 7                   >
     * 0 (Init)             everything else
     * </pre>
     * If the next read character is not '>' this state will cause the FSM to go two characters
     * back and create a SignToken for the former read '<'.
     *
 */
    void readChar(char c) {

        Log::stateRead(stateNr, c);
        //transition with '>'
        if (c == '>') {
            Log::stateReadResult(stateNr, 7, 1);
            fsm->setCurrentState(fsm->provideState(7));

        } else {
            Log::stateReadResult(stateNr, -1, 0);

            //go two chars back, next read char will be '<'
            fsm->ungetChar(2);
            //next char read will be ':' and next state will be 8
                       fsm->setCurrentState(fsm->provideState(0));
            //make a token
         fsm->makeToken(Tokens::TC_SIGN); //Token: <

        }
    }

};
/**
 * State07 is one of the states responsible for recognizing signs.
 *   This state is required to recognize the compound sign '<:>'.
 *  This state is reached from state 6 with the character '>'.
 *  It means, that the compound siogn '<:>' was recognized.
 * \author  Dominik Bartos
 */
class State07 : public StateBase {

public:
    /**
* Creates a new instance of the state
* @param FSM the FSM which uses the state
*
*/
    State07(FSM *fsm) : StateBase(fsm) { }

    ~State07() { };
/**

   * Reads a char and starts following transitions:<br>
     * <pre>
     * following state | triggering character
     * 0 (Init)             everything else
     * </pre>
     * This state will create a SignToken for the sign '<:>'
     *
 */
    void readChar(char c) {

        Log::stateRead(stateNr, c);
        Log::stateReadResult(stateNr, -1, 0);
        fsm->ungetChar(1);
        fsm->makeToken(Tokens::TokenCategory::TC_SIGN);
        fsm->setCurrentState(fsm->provideState(0));

    }

};
/**
 * State08 is one of the states responsible for recognizing signs.
 *     This state is reached from the InitState with the sign ':'. it is needed to recognize the
     * compound sign ':=' as well as comment blocks.
 * \author  Dominik Bartos
 */
class State08 : public StateBase {

public:
    /**
*@copydoc State03::State03(FSM* fsm)
*
*/
    State08(FSM *fsm) : StateBase(fsm) { }

    ~State08() { };
    /**

     *
     * Reads a char and starts following transitions:<br>
     * <pre>
     * following state | triggering character
     * =                    9
     * *                    10
     * 0 (InitState)        every character
     * </pre>
     * If a'=' is read, this means that the sign ':=' is recognized in state 9.
     * If a '*' is read a comment block is starting.
     * If any other type of character is read in here this state will create a SignToken for
     * '=' and will go back into InitState
     *
     */
    void readChar(char c) {

        Log::stateRead(stateNr, c);
        //transition with '='
        if (c == '=') {
            Log::stateReadResult(stateNr, 9, 1);
            fsm->setCurrentState(fsm->provideState(9));
        } else if (c == '*') {
            Log::stateReadResult(stateNr, 10, 1);
            fsm->setCurrentState(fsm->provideState(10));

        }
        else {
            Log::stateReadResult(stateNr, -1, 0);
            fsm->ungetChar(1); //going one char back. current char then will be ':'
            fsm->makeToken(Tokens::TokenCategory::TC_SIGN); //making a token of ':'
            fsm->setCurrentState(fsm->provideState(0));


        }


    }

};

/**
 * State09 is one of the states responsible for recognizing signs.
 * This state is reached from the InitState with the sign '='. it is needed to recognize the
   * compound sign ':=' as well as comment blocks.
 * \author  Dominik Bartos
 */
class State09 : public StateBase {

public:
    /**
*@copydoc State03::State03(FSM* fsm)
*
*/
    State09(FSM *fsm) : StateBase(fsm) { }

    ~State09() { };

    /**
   *
   *
   * Reads a char and starts following transitions:<br>
   * <pre>
   * following state | triggering character
   * 0 (InitState)        every character
   * </pre>
   *This state only recognizes '='. It will then create a token for the compound sign ':='
     * and move into InitState.
   */
    void readChar(char c) {

        Log::stateRead(stateNr, c);
        Log::stateReadResult(stateNr, -1, 0);
        fsm->ungetChar(1);
        fsm->makeToken(Tokens::TokenCategory::TC_SIGN);
        fsm->setCurrentState(fsm->provideState(0));


    }

};
/**
 * State10 is one of the states responsible for recognizing signs
 * This state is reached from State 08 with the sign '*?. it is needed to recognize comment blocks
 * \author  Dominik Bartos
 */
class State10 : public StateBase {

public:
    /**
*@copydoc State03::State03(FSM* fsm)
*
*/
    State10(FSM *fsm) : StateBase(fsm) { }

    ~State10() { };
    /**

    *
    * Reads a char and starts following transitions:<br>
    * <pre>
    * following state | triggering character
    * 0 (InitState)        '\0'
     * 11                   *
     * 10                   everything else
    * </pre>
    *This state only recognizes '='. It will then create a token for the compound sign ':='
      * and move into InitState.
    */
    void readChar(char c) {

        Log::stateRead(stateNr, c);
        //transition with everything except '*' and null on itself
        if (c == '*') {
            Log::stateReadResult(stateNr, 11, 1);
            fsm->setCurrentState(fsm->provideState(11));
        } else if (c == '\0') {
            Log::stateReadResult(stateNr, 2, 1);
            fsm->ungetChar(1); //go one char back, initstate will read EOF and will be going into stop state
            fsm->setCurrentState(fsm->provideState(0));
        } else {
            //loop
            Log::stateReadResult(stateNr, 10, 1);
            fsm->setCurrentState(fsm->provideState(10));
        }


    }

};
/**
 * State11 is one of the states responsible for recognizing signs
 * This state is reached from State 10 with the sign '*'.
 * This sign could indicate the beginning of the end comment block sign ('*:')
 * \author  Dominik Bartos
 */
class State11 : public StateBase {

public:
    /**
*@copydoc State03::State03(FSM* fsm)
*
*/
    State11(FSM *fsm) : StateBase(fsm) { }

    ~State11() { };
    /**

      *
      * Reads a char and starts following transitions:<br>
      * <pre>
      * following state | triggering character
      * 12                  : (end of comment)
      * 0 (Init)            '\0' (end of file)
      * 10               everything else (everything is allowed wthin a comment)
      * </pre>
      * No tokens are created here
      */
    void readChar(char c) {

        Log::stateRead(stateNr, c);
        if (c == ':') {
            Log::stateReadResult(stateNr, 12, 1);
            fsm->setCurrentState(fsm->provideState(12));
        } else if (c == '\0') {
            Log::stateReadResult(stateNr, 0, 1);
            fsm->ungetChar(1); //go one char back, initstate will read EOF and will be going into stop state
            fsm->setCurrentState(fsm->provideState(0));
        } else {
            Log::stateReadResult(stateNr, 10, 1);
            fsm->setCurrentState(fsm->provideState(10));
        }



    }

};
/**
 * State12 is one of the states responsible for recognizing signs
 * This state is reached from State 11 with the sign ':'.
 * This means that the end of a comment block was recognized.
 * \author  Dominik Bartos
 */
class State12 : public StateBase {

public:
    /**
*@copydoc State03::State03(FSM* fsm)
*
*/

    State12(FSM *fsm) : StateBase(fsm) { }

    ~State12() { };
/**

      *
      * Reads a char and starts following transitions:<br>
      * <pre>
      * following state | triggering character
      * 0 (Init)          every character
      * </pre>
      * Token with type NOTOKEN is created here to indicate that the comment is closed
      */
    void readChar(char c) {

        Log::stateRead(stateNr, c);
        Log::stateReadResult(stateNr, -1, 0);

        //no token must be created but go one char back (the char read in here does not belong to the comment
        //and is part of the next token
        fsm->ungetChar(1);
        fsm->makeToken(Tokens::TC_NOTOKEN);//only to determine, that a comment is finished
        fsm->setCurrentState(fsm->provideState(0));




    }

};

#endif //AUTOMAT_SIGNSTATES_H
