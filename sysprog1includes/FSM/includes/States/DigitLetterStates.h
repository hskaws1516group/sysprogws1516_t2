//
// Created by Dominik on 21.10.2015.
//

#ifndef AUTOMAT_DIGITSTATES_H
#define AUTOMAT_DIGITSTATES_H


#include "../Log.h"
/**
 * State04 recognizes integers.
 * It is reached from InitState with a single digit (0-9).
 * \author  Dominik Bartos
 */
class State04 : public StateBase{

public:
    /**
     * Creates a new instance of the state
     * @param FSM the FSM which uses the state
     *
     */
    State04(FSM *fsm) : StateBase(fsm) { }

    ~State04(){};
    /**
       * Reads a char and starts following transitions:<br>
       * <pre>
       * following state | triggering character
       * 4                      digit (0-9)
       * 0                      everything else
       * </pre>
       * If a character which is not a digit is read, a IntegerToken will be created.
       */
   void readChar(char c){

        Log::stateRead(stateNr, c);
        int asciiCode = (unsigned char) c;
        //reading arbitrary number of digits
        if(asciiCode>=48 && asciiCode <= 57){
            Log::stateReadResult(stateNr,4, 1);

            fsm->setCurrentState(fsm->provideState(4));
        }else {
            Log::stateReadResult(stateNr,-1, 0);
           fsm->ungetChar(1);
            fsm->makeToken(Tokens::TokenCategory::TC_INTEGER);
            fsm->setCurrentState(fsm->provideState(0));
        }
    }

};
/**
 * State04 recognizes identifiers.
 * It is reached from InitState with a single letter (a-z, A-Z).
 * \author  Dominik Bartos
 */
class State13 : public StateBase{

public:
    /**
  * Creates a new instance of the state
  * @param FSM the FSM which uses the state
  *
  */
    State13(FSM *fsm) : StateBase(fsm) {


    }

    ~State13(){};

    /**
      * Reads a char and starts following transitions:<br>
      * <pre>
      * following state | triggering character
      * 13                      letter (a-z, A-Z)
      * 13                      digit (0-9)
      * 0 (Init)                everything else
      * </pre>
      * If a character which is not a digit/letter is read, a IntegerToken will be created.
      */
    void readChar(char c){


        Log::stateRead(stateNr, c);
        int asciiCode = (unsigned char) c;
        if((asciiCode >=65 && asciiCode <= 90) || (asciiCode >= 97 && asciiCode <= 122) || (asciiCode>=48 && asciiCode <= 57 )) {
           Log::stateReadResult(stateNr,13, 1);

            fsm->setCurrentState(fsm->provideState(13));

        }else {
           Log::stateReadResult(stateNr,-1, 0);
            fsm->ungetChar(1);
            fsm->makeToken(Tokens::TokenCategory::TC_IDENTIFIER);
            fsm->setCurrentState(fsm->provideState(0));


        }
    }

};
#endif //AUTOMAT_DIGITSTATES_H
