//
// Created by Dominik on 21.10.2015.
//

#ifndef AUTOMAT_LFSM_H
#define AUTOMAT_LFSM_H

#include "../includes/FSM.h"


#include "../../Scanner/includes/IScanner.h"

/**
 * The LFSM is a specific implementation of a FSM.
 * ('L' stands for the language which it can recognize. In the context of the Scanner project 'L' is out language which
 * is used).
 * The LFSM can distinguish when a token needs to be created and can provide line, column and length of the token.
 */
class LFSM : public FSM {

public:
    /**
     * Creates a new instance of the LFSM. The LFSM need to know the scanner in which it works.
     * So it will be provided here.
     * @param scanner the scanner which holds the lfsm.
     */
    LFSM(IScanner *scanner);

    /**
     * Destroys the LFSM and releases used ressources.
     */
    virtual ~LFSM();

    /**
     * Resets the LFSM on its inital state.
     */
    void reset();

    /**
     * Returns the actuial position within a line.
     */
    int getLinePosition();

    /**
     * Returns the actual row.
     */
    int getRowCount();

    /**
     * Returns the length of the last recognized token.
     */
    int getActualTokenLength();

/**
 * Provides a charatcer to LFSM which is then processed by its states.
 * @param c the character to process.
 */
    virtual void readChar(char c) override;

    /**
       * Returns a state that is associated with the given stateNr.
       * @param StateBase the state which corresponds to the stateNr.
       */
    virtual StateBase *provideState(int stateNr) override;

    /**
   * Indicates to go a specific number of chars back
   * @param nr the number of chars to go back
   */
    virtual void ungetChar(int nr) override;

    /**
 * Called if a token was recognized by the fsm.
 * The tokenType specifies the type of the token. It depends on which state
 * was the last state in the fsm.
 * @param tokenType the type of the recognized token
 */
    virtual void makeToken(Tokens::TokenCategory tokenType) override;

private:
    IScanner *scanner;
    const int stateCount = 15;
    StateBase **states = new StateBase *[stateCount];

    int *linePosHistory;
    int linePosHistoryCounter;
    int *rowPosHistory;
    int rowPosHistoryCounter;
    int linePosition = 0;
    int rowCount = 0;






};

#endif //AUTOMAT_LFSM_H
