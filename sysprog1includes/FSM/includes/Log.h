//
// Created by Dominik on 21.10.2015.
//

#ifndef AUTOMAT_LOG_H
#define AUTOMAT_LOG_H
#undef TEST_MODE

//#define TEST_MODE

/**
 * This class provides functions to print the actions of a FSM out on console.
 * The functions are static and inline. They are only active, if the flag 'TEST_MODE' is defined, otherwise they won't work.
 * (preprocessor switch: if TEST_MODe is not defined, the functions will be empty). They are also inlined to prevent
 * performance hit while used in a FSM.
 */
class Log {
public:
    /**
     * Print a plain message onto console.
     * @param debugMessage the message to print
     */
    static inline void debug(char *debugMessage) {
#ifdef TEST_MODE
        printf("%s\n", debugMessage);
#endif
    }

    /**
     * Prints the actula state and the actual readChar
     * @param currentState the number of the currentState
     * @param readChar the character which was read
     */
    inline static void stateRead(int currentState, char readChar) {
#ifdef TEST_MODE
        printf("State %i read \"%c\":\n", currentState, readChar);
#endif
    }

    /**
     * Prints the result after processing a character.
     * @param currentState the current state
     * @param nextStateNr the number of the followoing state
     * @param result resultcode. value greater that 0 indicates that there is a transition, value equal or less 0
     * indicated, that there is no transition with the given character
     */
    inline static void stateReadResult(int currentState, int nextStateNr, int result) {

#ifdef TEST_MODE

        if(result > 0){
            printf("State %i going to state %i. \n", currentState, nextStateNr);
        }else {
            printf("State %i no transition (%i). \n", currentState, nextStateNr);
        }
#endif

    }
};

#endif //AUTOMAT_LOG_H
