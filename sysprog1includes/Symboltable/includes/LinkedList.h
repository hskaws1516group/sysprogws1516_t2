/*
 * LinkedListE.h
 *
 *  Created on: 11.11.2015
 *      Author: benjamin
 */

#ifndef SYMBOLTABLE_SRC_LINKEDLISTE_H_
#define SYMBOLTABLE_SRC_LINKEDLISTE_H_

template <class Value>
/**
 * A simple generic Linked List storing key-value-pairs.
 * Value is generic, key is fixed to char*.
 * @author Benjamin Büscher
 */
class LinkedList {
public:

	/**
	 * Creates an instance of LinkedList.
	 */
	LinkedList();

	/**
	 * deletes the LinkedList.
	 */
	virtual ~LinkedList();

	/**
	 * Adds the key-value pair.
	 * Doesn't check for collisions.
	 * @param key the key. Doesn't get checked for collisions.
	 * @param value the value to be stored associated with the key.
	 */
	void add(const char* key, Value* value);

	/**
	 * Returns the last value associated with the given key.
	 * @param key the key to get an associated value to.
	 * @return the last added value associated with the given key
	 */
	Value* get(const char* key);

	/**
	 * checks if the List contains a key-value pair with the given key.
	 * @param key key to be searched for.
	 * @return true if the list contains a fitting key-value pair.
	 */
	bool exists(const char* key);

	/**
	 * Counts the number of key-value pairs stored in this list.
	 * @return the number of elements in this list.
	 */
	int countElements();

private:
	class ListElement {
	public:
		ListElement(const char* key, Value* value);
		virtual ~ListElement();

		ListElement* next;
		const char* key;
		Value* value;
	};

	ListElement* first;
};
//#include "../src/LinkedList.cpp"
#endif /* SYMBOLTABLE_SRC_LINKEDLISTE_H_ */
