/*
 * Symboltable.h
 *
 *  Created on: 12.11.2015
 *      Author: benjamin
 */

#ifndef SYMBOLTABLE_SRC_SYMBOLTABLE_H_
#define SYMBOLTABLE_SRC_SYMBOLTABLE_H_
#include "Hashtable.h"
#include "../../Token/includes/Token.h"

/**
 * A hashtable storing lexem-token pairs.
 * @author Benjamin Büscher
 */
class Symboltable {
public:

	/**
	 * Creates a new symboltable with the given size.
	 * @param size the size for the symboltable
	 */
	Symboltable(int size);

	/**
	 * deletes the symboltable
	 */
	virtual ~Symboltable();

	/**
	 * checks if the symboltable contains a lexem-token pair with the given lexem.
	 * @param lexem lexem to be searched for.
	 * @return true if the symboltable contains a fitting lexem-token pair.
	 */
	bool contains(const char* lexem);

	/**
	 * Inserts the lexem-token pair in the symboltable.
	 * If the symboltable already contains the lexem nothing is done.
	 * @param lexem the lexem to be associated with the token.
	 * @param token the token to be stored associated with the lexem.
	 */
	void insert(const char* lexem, Token* token);

	/**
	 * Returns the token associated with the given lexem.
	 * @param lexem the lexem to get associated token to.
	 * @return the token associated with the given lexem. null if key doesn't exist.
	 */
	Token* lookup(const char* lexem);

	/**
	 * prints stats of the hashtable to cout.
	 */
	void printStats();
private:
	Hashtable<Token>* symtable;
};

#endif /* SYMBOLTABLE_SRC_SYMBOLTABLE_H_ */
