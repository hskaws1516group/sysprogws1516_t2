/*
 * Scanner.h
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */

#ifndef SCANNER_H_
#define SCANNER_H_

#include "../../Token/includes/TokenBuilder.h"
#include "../../Buffer/includes/Buffer.h"
#include "../../FSM/includes/LFSM.h"
#include "../../Symboltable/includes/Symboltable.h"
#include "../../Token/includes/Token.h"
#include "IScanner.h"

/**
 * The scanner reads a given text file and splits the content into tokens.
 */
class Scanner : IScanner{
public:
	/**
	 * Creates a new instance and initializes the buffer, tokenBuilder and 
	 * state machine
	 * @param filename Path to the input file
	 */
	Scanner(const char* filename, Symboltable* symtab);
	/**
	 * Destructor
	 */
	virtual ~Scanner();
	/**
	 * Get informaion using the state machine and allocate memory for the token.
	 * Build the token with the tokenBuilder class, handle errors and write an
	 * entry into the SymbolTable.
	 * @param tokenCatory Category of the token (IDENTIFIER / INTEGER / ...)
	 */
	void createToken(Tokens::TokenCategory tokenCategory);
	/**
	 * Move the buffers internal pointer x characters back
	 * @param nr Number of characers
	 */
	void ungetChar(int nr) ;

	/**
	 * Initilizes the scanner.
	 */
	  void init();

	/**
	 * Processes characters from  the given textfile as long as there is no token found. If a token was found, it will be
	 * returned.
	 * @return a token, if found
	 */
	  Token* nextToken() ;

	/**
	 * Returns the state of the file
	 * @return true, if the file is at its end, false if not.
	 */
	  bool isFileEnd();



private:

	Buffer*			buffer;
	TokenBuilder* 	tokenBuilder;
	LFSM* fsm;
    Symboltable* symtab;
    bool tokenFound = false;
    Token* foundToken;
    bool fileEnd = false;




};

#endif /* SCANNER_H_ */
