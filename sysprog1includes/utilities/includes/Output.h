/*
 * Output.h
 *
 *  Created on: 09.12.2015
 *      Author: benjamin
 */

#ifndef UTILITIES_SRC_OUTPUT_H_
#define UTILITIES_SRC_OUTPUT_H_
#include <fstream>
#include "../../Token/includes/Token.h"

/**
 * Output manages the output of tokens to file and cerr
 * \author Benjamin Büscher
 */
class Output {
public:

	/**
	 * Writes Buffers to the output file and the error stream and closes the file.
	 */
	static void close();

	/**
	 * Opens the connection to the file and sets up the streams.
	 * If file doesn't exist yet it's created automatically.
	 * @param filePath the path to the output file.
	 */
	static void init(const char* filePath);

	/**
	 * Prints the token to the file.
	 * Buffer doesn't get flushed.
	 * @param token the token to be printed
	 */
	static void printToken(Token* token);

	/**
	 * Prints the token to the error stream.
	 * @param token the token to be printed
	 */
	static void printErrorToken(Token* token);

	virtual ~Output();
private:
	/**
	 * No constructor needed, everything is static.
	 */
	Output();

	static std::ofstream file;
};

#endif /* UTILITIES_SRC_OUTPUT_H_ */
