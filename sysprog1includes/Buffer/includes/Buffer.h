/*
 * Buffer.h
 *
 *  Created on: Sep 26, 2012
 *      Author: knad0001
 */

#ifndef BUFFER_H_
#define BUFFER_H_


/**
 * Class that handles file operations. Reads from a file using open / read
 * with the flag O_DIRECT. Allocates memory for two internal buffers to speed up file
 * operations
 */
class Buffer {
public:
	/**
	 * Creates a new instance
	 * @param filename Path to the input file
	 */
	Buffer(const char* filename);
	/**
	 * Destructer
	 * Free memory of the internal buffers
	 */
	virtual ~Buffer();
	/**
	 * Returns the next char of the input file. Returns NULL when the end
	 * of the file has arrived
	 * @return The next char
	 */
	char getChar();
	/**
	 * Set the internal pointer 1 character back
	 */
	void ungetChar();
	/**
	 * Takes an allocated space as param one and writes the last x characters
	 * into it
	 * @param dest Pointer to an allocated space
	 * @param charCount Amout of chars that are copied
	 */
	void copyLastCharactersToMemory(char* dest, int charCount);

private:
	const char* filename;

	char* buffer1;
	char* buffer2;
	char* activeBuffer;

	int index;
	int fileHandle;
	int bufferRound;
	int buffer1DataSize;
	int buffer2DataSize;
	
	bool prevBufferAvailable;
	bool nextBufferAvailable;

	int getBufferSize(char* buffer);
	void loadNextBuffer(char* buffer, bool setActive);
	void resetToBufferRound(int round);
	void openFile();
};

#endif /* BUFFER_H_ */
