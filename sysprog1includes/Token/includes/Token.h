//
// Created by Dominik on 02.11.2015.
//

#ifndef SYSPROGWS1516_TOKEN_H
#define SYSPROGWS1516_TOKEN_H

#include "TokenTypes.h"
#include <iostream>
#include "../../../tree/includes/InformationType.h"

/**
 * A token contains information about a found token in the textfile.
 * It contains the position of the token in the textfile (line and row) and the tokenstring.
 * A token can be of different type: identifier, sign or integer.
 * If the token is an sign token, it containst the type of the containing sign.
 * If the token is a identifier token, it contains the identifier as string.
 * If the token is an integer, it contains the value of the inteher.
 */
    class Token {

    public:


        /**
         * Creates a new instance of the token class. Usually used for sign tokens.
         * @param type the type of the token
         * @param line the line which contains the token
         * @param column the column in which the token starts in the file
         */
        Token(Tokens::TokenType type, int line, int column);
        /**
         * Creates a new instance of the token class. Usually used for integer tokens.
         * @param line the line which contains the token
         * @param column the column in which the token starts in the file
         * @integerValue the value of the integer,that was found in the file
         */
        Token(int line, int column, int integerValue);
        /**
      * Creates a new instance of the token class. Usually used for identifier tokens.
      * @param type the type of the token
      * @param line the line which contains the token
      * @param column the column in which the token starts in the file
         *  @param identifier the identifier which was found in the file
      */
        Token(Tokens::TokenType type, int line, int column, const char* identifier);

        /**
         * Destroys the token and releases resources
         */
        ~Token();
        /**
         * returns the type of the token.
         */
        Tokens::TokenType getType();


        /**
         * Contains the names of the different tokens (especially signs) for visual output.
         */
        static const char* const tokenTypeNames[] ;

        /**
         * Makes <<-operator friend, to get directyl and out stream with the tokendata by using <<-operator
         */
        friend std::ostream& operator<< (std::ostream &out, Token &token);

        /**
         * Returns the integer value of the token
         */
        int getIntegerValue();

        /**
         * Returns the identifier
         */
        const char* getIdentifier();

        /**
         * prints only the most important info
         */
        void printSimple();


        void setInfoType(InformationType::InfoType infoType);
        InformationType::InfoType getInfoType();

    private:
        Tokens::TokenType type;
        int line;
        int column;

        const char* identifierName;
        int integerValue;

        static const int maxTokenNameWidth;
        InformationType::InfoType infoType;

    };



#endif //SYSPROGWS1516_TOKEN_H
