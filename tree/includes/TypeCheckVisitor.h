//
// Created by Dominik on 16.01.2016.
//

#ifndef SYSPROGWS1516_T2_TYPECHECKVISITOR_H
#define SYSPROGWS1516_T2_TYPECHECKVISITOR_H
#include "TreeVisitor.h"
#include "ParseTreeNode.h"
#include "../../sysprog1includes/Symboltable/includes/Symboltable.h"

class TypeCheckVisitor : public TreeVisitor {
class TreeVisitor;
public:
    TypeCheckVisitor(Symboltable* symtab);
    virtual void visitNode(ParseTreeNode* node) override;
private:
    void error(char* message, ParseTreeNode* node);
    void store(const char* identifier, InformationType::InfoType infoType);
    InformationType::InfoType getType(const char* identifier);
    Symboltable* symtab;
};

#endif //SYSPROGWS1516_T2_TYPECHECKVISITOR_H
