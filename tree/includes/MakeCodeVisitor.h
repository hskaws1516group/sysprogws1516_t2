//
// Created by Dominik on 16.01.2016.
//

#ifndef SYSPROGWS1516_T2_MAKECODEVISITOR_H
#define SYSPROGWS1516_T2_MAKECODEVISITOR_H
#include "TreeVisitor.h"
#include "ParseTreeNode.h"
class MakeCodeVisitor : public TreeVisitor {
class TreeVisitor;
public:
    MakeCodeVisitor(std::ostream &code);
    virtual void visitNode(ParseTreeNode* node) override;



private:
    std::ostream& code;
    unsigned int labelNumber;
    unsigned int getLabelNumber();
};

#endif //SYSPROGWS1516_T2_MAKECODEVISITOR_H
