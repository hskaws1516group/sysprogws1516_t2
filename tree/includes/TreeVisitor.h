//
// Created by Dominik on 16.01.2016.
//

#ifndef SYSPROGWS1516_T2_TREEVISITOR_H
#define SYSPROGWS1516_T2_TREEVISITOR_H

#include "ParseTreeNode.h"
class ParseTreeNode;
class TreeVisitor {
public:
   virtual void visitNode(ParseTreeNode* node) = 0;
};

#endif //SYSPROGWS1516_T2_TREEVISITOR_H
