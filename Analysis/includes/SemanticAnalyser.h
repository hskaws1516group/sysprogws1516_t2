//
// Created by Dominik on 29.12.2015.
//

#ifndef SYSPROGWS1516_T2_TYPECHECKER_H
#define SYSPROGWS1516_T2_TYPECHECKER_H

#include "../../tree/includes/RuleTypes.h"

class SemanticAnalyser {
public:
    void typeCheck(Rules::RuleType rule);
};

#endif //SYSPROGWS1516_T2_TYPECHECKER_H
